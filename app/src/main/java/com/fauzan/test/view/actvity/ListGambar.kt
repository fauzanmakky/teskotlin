package com.fauzan.test.view.actvity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.fauzan.test.R
import com.fauzan.test.model.ModelGambar
import com.fauzan.test.repo.GambarRepo
import com.fauzan.test.service.BaseApi
import com.fauzan.test.view.adapter.ListGambarAdapter
import com.fauzan.test.view.util.Toast
import com.fauzan.test.view.util.gone
import com.fauzan.test.view.util.showSnackbarBottom
import com.fauzan.test.view.util.visible
import kotlinx.android.synthetic.main.activity_list_gambar.*

class ListGambar : AppCompatActivity(), ListGambarContact.ListGambarView {
    lateinit var listGambarAdapter: ListGambarAdapter
    lateinit var gambarRepo: GambarRepo
    lateinit var mPresenterImp: ListGambarPresenterImp

    override fun addData(modelGambar: ModelGambar) {
        listGambarAdapter.addDAta(modelGambar)
    }

    override fun setDataAdapter(modelGambars: MutableList<ModelGambar>) {
        listGambarAdapter = ListGambarAdapter(modelGambars)
        main_list_utama.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = listGambarAdapter
        }
    }

    override fun onError() {
        showSnackbarBottom(maint_parent, "Terjadi Kesalahan").show()
    }

    override fun onErroMessage(message: String) {
        Toast(this, message).show()
    }

    override fun showProgres() {
        main_progres.visible()
    }

    override fun closeProgres() {
        main_progres.gone()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_gambar)
        val factory: BaseApi = BaseApi.create()
        gambarRepo = GambarRepo(factory)
        mPresenterImp = ListGambarPresenterImp(this, gambarRepo, this)
        mPresenterImp.getData()


    }
}
