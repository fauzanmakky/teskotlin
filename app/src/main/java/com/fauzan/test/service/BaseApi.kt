package com.fauzan.test.service

import com.fauzan.test.model.ResponGambar
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface BaseApi {


    @GET("yoesuv/0c274f3314cefd40f66e6ed83f08acc6/raw/8b0c6eb6a95cde6db904f5a0eddba280aef96680/ListData.json")
    fun getGambar(): Deferred<ResponGambar>

    companion object {

        fun create(): BaseApi {

            val gson = GsonBuilder()
                .setLenient()
                .create()
            val clientBuilder = OkHttpClient.Builder()
            clientBuilder.connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .build()
            val client = clientBuilder.build()
            val retrofit = Retrofit.Builder()
                .baseUrl("https://gist.githubusercontent.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(BaseApi::class.java)
        }
    }
}