package com.fauzan.test.view.adapter

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.fauzan.test.R
import com.fauzan.test.model.ModelGambar
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_gambar.view.*

class MediaAdapter(val mediaList: MutableList<ModelGambar.Media>) :
    RecyclerView.Adapter<MediaAdapter.MediaViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MediaViewHolder {
        val view = LayoutInflater.from(p0.getContext())
            .inflate(R.layout.item_gambar, p0, false)
        return MediaViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mediaList.size
    }

    override fun onBindViewHolder(p0: MediaViewHolder, p1: Int) {
        Picasso.get().load(mediaList[p1].url).into(p0.gambar)
    }


    class MediaViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val gambar: ImageView = view.item_gambar_gambar

    }
}