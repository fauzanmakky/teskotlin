package com.fauzan.test.repo

import com.fauzan.test.model.ResponGambar
import com.fauzan.test.service.BaseApi
import kotlinx.coroutines.Deferred

class GambarRepo(private val baseApi: BaseApi) {
    fun loadGambar(): Deferred<ResponGambar> {
        return baseApi.getGambar()
    }
}