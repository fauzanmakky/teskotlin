package com.fauzan.test.view.adapter

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.fauzan.test.R
import com.fauzan.test.model.ModelGambar
import com.squareup.picasso.Picasso

class ListGambarAdapter(val modelGambars: MutableList<ModelGambar>?) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val TIPE1 = 1
    private val TIPE2 = 2
    lateinit var context: Context
    lateinit var mediaAdapter: MediaAdapter
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        context=p0.context
        when (p1) {
            TIPE1 -> {
                val view = LayoutInflater.from(p0.getContext())
                    .inflate(R.layout.item_media, p0, false)
                return MediaViewHolder(view)

            }

            TIPE2 -> {
                val view1 = LayoutInflater.from(p0.getContext())
                    .inflate(R.layout.item_multi_media, p0, false)
                return MultiMediaViewHolder(view1)
            }
            else -> {
                val view = LayoutInflater.from(p0.getContext())
                    .inflate(R.layout.item_media, p0, false)
                return MediaViewHolder(view)
            }
        }
    }

    override fun getItemCount(): Int {
        return modelGambars?.size!!
    }

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, position: Int) {
        val tipe = getItemViewType(position)
        when (tipe) {
            TIPE1 -> {

                val mediaViewHolder = p0 as MediaViewHolder
                mediaViewHolder.title.setText(modelGambars?.get(position)?.title)
                mediaViewHolder.content.setText(modelGambars?.get(position)?.content)
                Picasso.get().load(modelGambars?.get(position)?.media?.get(0)?.url)
                    .into(mediaViewHolder.image)
            }
            TIPE2 -> {
                val multiMediaViewHolder = p0 as MultiMediaViewHolder
                multiMediaViewHolder.multititle.setText(modelGambars?.get(position)?.title)
                multiMediaViewHolder.multicontent.setText(modelGambars?.get(position)?.content)
                mediaAdapter = MediaAdapter(modelGambars?.get(position)?.media as MutableList<ModelGambar.Media>)
                val linearLayoutManager =
                    LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                multiMediaViewHolder.listMedia.layoutManager = linearLayoutManager
                multiMediaViewHolder.listMedia.adapter = mediaAdapter
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        val tipe: Int

        if (modelGambars?.get(position)?.type.equals("multiple")) {
            tipe = 2
        } else
            tipe = 1

        return tipe

    }

    fun addDAta(modelGambar: ModelGambar) {
        modelGambars?.add(modelGambar)
        notifyItemInserted(modelGambars?.size!!)
    }

}

class MediaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    internal var content: TextView
    internal var title: TextView
    internal var image: ImageView

    init {
        content = itemView.findViewById(R.id.item_media_content)
        image = itemView.findViewById(R.id.item_media_gambar)
        title = itemView.findViewById(R.id.item_media_titlle)

    }
}

class MultiMediaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    internal var multicontent: TextView=itemView.findViewById(R.id.item_multi_content)
    internal var multititle: TextView=itemView.findViewById(R.id.item_multi_title)
    internal var listMedia: RecyclerView=itemView.findViewById(R.id.item_multi_recyler)

}
