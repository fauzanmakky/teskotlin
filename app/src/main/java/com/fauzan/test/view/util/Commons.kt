package com.fauzan.test.view.util

import android.content.Context
import android.net.ConnectivityManager
import android.support.design.widget.Snackbar
import android.view.View
import android.widget.Toast


fun isConnection(context: Context): Boolean {
    val cm: ConnectivityManager?
    cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    assert(cm != null)
    val info = cm.activeNetworkInfo
    return info != null && info.isConnected
}

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun showSnackbarBottom(view: View, msg: String): Snackbar {
    return Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
}

fun Toast(context: Context, msg: String): Toast {
    return Toast.makeText(context, msg, Toast.LENGTH_SHORT)
}