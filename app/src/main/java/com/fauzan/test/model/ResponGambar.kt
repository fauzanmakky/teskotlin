package com.fauzan.test.model

import com.google.gson.annotations.SerializedName

class ResponGambar {
    @SerializedName("status_code")
    var statusCode: Int? = null
    @SerializedName("data")
    var data: List<Data>? = null

    inner class Data {

        @SerializedName("id")
        var id: Int? = null
        @SerializedName("title")
        var title: String? = null
        @SerializedName("content")
        var content: String? = null
        @SerializedName("type")
        var type: String? = null
        @SerializedName("media")
        var media: List<String>? = null

    }
}