package com.fauzan.test.view.actvity

import com.fauzan.test.model.ModelGambar

interface ListGambarContact {
    interface ListGambarView {
        fun addData(modelGambar: ModelGambar)

        fun setDataAdapter(modelGambars: MutableList<ModelGambar>)

        fun onError()

        fun onErroMessage(message: String)

        fun showProgres()

        fun closeProgres()

    }

    interface ListGambarPresenter {
        fun getData()
    }
}