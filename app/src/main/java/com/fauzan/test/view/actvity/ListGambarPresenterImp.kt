package com.fauzan.test.view.actvity

import android.content.Context
import com.fauzan.test.model.ModelGambar
import com.fauzan.test.repo.GambarRepo
import com.fauzan.test.view.util.isConnection
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.net.SocketTimeoutException
import java.util.ArrayList

class ListGambarPresenterImp(
    var mView: ListGambarContact.ListGambarView,
    var repo: GambarRepo,
    var context: Context
) :
    ListGambarContact.ListGambarPresenter {
    internal var mediaList: MutableList<ModelGambar.Media>? = null
    val modelGambar: MutableList<ModelGambar> = arrayListOf()
    var kali = 0
    override fun getData() {
        if (isConnection(context)) {
            mView.setDataAdapter(modelGambar)
            loadDataImage()
        } else {
            mView.onErroMessage("Tidak Ada Koneksi Internet")
        }
    }

    private fun loadDataImage() {
        mView.showProgres()
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val result = repo.loadGambar().await()
                if (result != null && result.statusCode == 200) {

                    var media: ModelGambar.Media?
                    if (result.data != null) {
                        for (i in 0 until result.data?.size!!) {
                            mediaList = ArrayList()

                            for (j in result.data?.get(i)?.media?.indices!!) {
                                media = ModelGambar.Media(result.data?.get(i)?.media?.get(j))
                                mediaList?.add(media)
                            }

                            val modelGambar = ModelGambar(
                                result.data?.get(i)?.id,
                                result.data?.get(i)?.title,
                                result.data?.get(i)?.content,
                                result.data?.get(i)?.type,
                                mediaList
                            )
                            mView.addData(modelGambar)

                        }
                        mView.closeProgres()
                    }

                }
            } catch (e: SocketTimeoutException) {
                if (kali == 0 && isConnection(context)) {
                    loadDataImage()
                    kali += 1
                } else {
                    mView.onErroMessage("Periksa Jaringan Internet Anda")
                    mView.closeProgres()
                }
            } catch (e: Throwable) {
                mView.onError()
                mView.closeProgres()
            }

        }
    }
}


